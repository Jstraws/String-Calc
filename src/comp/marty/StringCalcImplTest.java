package comp.marty;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



class StringCalcImplTest {

    StringCalcImpl sc = new StringCalcImpl();

    @Test
    void addEmptyTest() {
        String testString = "";
        int added = sc.Add(testString);
        assertEquals(0,added, "Does find empty");
    }

    @Test
    void addOneTest(){
        String testString = "1";
        int added = sc.Add(testString);
        assertEquals(1, added, "Does not add one number" );
    }

    @Test
    void addOneTest2(){
        String testString = "100";
        int added = sc.Add(testString);
        assertEquals(100, added, "Cannot add a number with mulitple digits" );
    }

    @Test
    void addOneTest3() {
        String testString = "-1";
        int sum = sc.Add(testString);
        assertEquals(-1, sum);
    }

    @Test
    void addTwoTest(){
        String testString = "2,3";
        int added = sc.Add(testString);
        assertEquals(5, added, "Does not add two numbers" );
    }

    @Test
    void addTwoTest2(){
        String testString = "0,0";
        int added = sc.Add(testString);
        assertEquals(0, added, "Cannot add two nums to 0");
    }

    @Test
    void addTwoTest3(){
        String testString = "100,10";
        int added = sc.Add(testString);
        assertEquals(110, added, "Cannot add numbers with mulitple digits" );
    }

    @Test
    void addTwoTest4() {
        String testString = "-10,3040";
        int sum = sc.Add(testString);
        assertEquals(3030, sum);
    }

    @Test
    void addManyTest(){
        String testString = "1,2,3";
        int added = sc.Add(testString);
        assertEquals(6, added, "Cannot add more than two numbers" );
    }

    @Test
    void addManyTest2() {
        String testString = "1,2,-3";
        int sum = sc.Add(testString);
        assertEquals(0, sum);
    }

    @Test
    void canSpaceTest(){
        String testString = "1\n2\n3";
        int added = sc.Add(testString);
        assertEquals(6, added, "Cannot add with space delimiter");
    }

    @Test
    void canBothTest(){
        String testString = "1,2\n3";
        int added = sc.Add(testString);
        assertEquals(6, added, "Cannot add with space delimiter and comma");
    }


    void addNewlineTest() {
        String testString = "1\n2,4";
        int sum = sc.Add(testString);
        assertEquals(7, sum);
    }

    @Test
    void addNewLineTest2() {
        String testString = "10,4\n16";
        int sum = sc.Add(testString);
        assertEquals(30, sum);
    }

}