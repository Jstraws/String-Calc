package comp.marty;

import java.util.List;
import java.util.ArrayList;


public class StringCalcImpl implements StringCalcInt {

    @Override
    public int Add(String numbers){

        if (numbers == ""){
            return 0;
        }

        String [] tobeParsed = numbers.split(",|\n");

        int total = 0;


        List<Integer> tobeAdded = new ArrayList<Integer>();

        for(int i = 0; i < tobeParsed.length; i++){
            tobeAdded.add(Integer.parseInt(tobeParsed[i]));
        }

        for(int i = 0; i < tobeAdded.size(); i++){
            total += tobeAdded.get(i);
        }

        return total;
    }
}
